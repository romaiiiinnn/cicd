# CI / CD

[![GitLab CI](https://gitlab.com/romaiiiinnn/cicd/badges/main/pipeline.svg)](https://gitlab.com/romaiiiinnn/cicd/-/commits/main)

Several `gitlab-ci` configuration templates to use in your projects.

## Example

```yaml
include: 
  - project: 'romaiiiinnn/cicd'
    ref: main
    file: 'hugo.yaml'

build:
  extends: .build_hugo
  variables:
    DESTINATION: public
  artifacts:
    paths:
      - $DESTINATION
    expire_in: 10min
```

## Support

Got questions?

You could [open an issue here](https://gitlab.com/romaiiiinnn/cicd/-/issues).

## Contributing

This is an active open-source project. I am always open to people who want to use the code or contribute to it.

Thank you for being involved! :heart_eyes:

## Authors & contributors

The original setup of this repository is by [Romain RICHARD](https://gitlab.com/romaiiiinnn).

## License

Every details about licence are in a [separate document](LICENSE).
